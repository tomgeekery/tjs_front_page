<?php

/**
 * Front page form for tjs front page module.
 */

/**
 * Front page content form.
 */
function tjs_front_page_content($form, &$form_state) {
  $form = array();

  $parameters = drupal_get_query_parameters();

  $form['call_to_action'] = array(
    '#type' => 'fieldset',
    '#title' => t('Call to action content'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['call_to_action']['call_to_action_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Call to action title'),
    '#default_value' => variable_get('call_to_action_title'),
  );

  $form['call_to_action']['call_to_action_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Call to action body'),
    '#default_value' => variable_get('call_to_action_body', ''),
  );

  $form['call_to_action']['call_to_action_image'] = array(
    '#title' => t('Call to action image'),
    '#type' => 'managed_file',
    '#default_value' => variable_get('call_to_action_image', ''),
    '#upload_location' => 'public://call_to_action_images/',
  );

  $form['front_page'] = array(
    '#type' => 'fieldset',
    '#title' => t('Front page content'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['front_page']['front_page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Front page title'),
    '#default_value' => variable_get('front_page_title', ''),
  );

  $form['front_page']['front_page_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Front page body'),
    '#default_value' => variable_get('front_page_body', ''),
  );

  $form['front_page']['front_page_image'] = array(
    '#title' => t('Front page image'),
    '#type' => 'managed_file',
    '#default_value' => variable_get('front_page_image', ''),
    '#upload_location' => 'public://front_page_images/',
  );

  return system_settings_form($form);
}
